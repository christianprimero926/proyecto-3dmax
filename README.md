Proyecto N°1

Computación Gráfica

El objetivo de este primer proyecto es evidenciar su conocimiento de transformaciones y proyecciones en el área de la computación gráfica. Para esto, podrá desarrollar este trabajo en un software de modelado 3d de su elección.
1. Seleccionar un sitio de su ciudad (o algún sitio famoso del mundo).
2. Obtener fotos reales del sitio.
3. Realizar un modelado 3D del sitio, enfocándose en los objetos representativos del mismo. Aunque no se pide exactitud en el modelado, los detalles (cantidad de objetos y uso de texturas) serán un punto de evaluación de la calidad de su trabajo.
4. Las imágenes reales del sitio deben ser incluidas en cuadros ubicados partes aleatorias dentro de su modelado 3D.
